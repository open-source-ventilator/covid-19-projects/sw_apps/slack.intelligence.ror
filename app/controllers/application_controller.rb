require 'csv'
require 'json'

class ApplicationController < ActionController::Base
    @@cols = []
    @@messages = []
    @@file = ""

    def upload
        @@file = File.open(params[:file].path).read
        redirect_to '/' 
    end

    def index
        if @@file.present?
            csv_string = CSV.generate do |csv|
                JSON.parse(@@file).each do |hash|
                    if hash[0] == "messages"
                        @@messages = hash[1]
                        hash[1].each do |message|
                            @@cols.push(message.keys)
                        end
                    end
                end
            end
        end
        
        @@cols = @@cols.flatten.uniq
        
        @table_headers = @@cols
        @cols = @@cols.collect{ |col| {"data" => col} }.to_json
    end

    def data
        data = {
            "data": @@messages
        }
        render json: data
    end
end
