Rails.application.routes.draw do
  get '/', to: 'application#index'
  post '/upload', to: 'application#upload'
  get 'data', to: 'application#data'
end
